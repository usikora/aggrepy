class Snippet():
    def __iter__( self ):
        return iter( self.resources ) 

    def __repr__(self):
        return f"<{self.__class__.__name__} { self._metadata }>"
    
    def __getitem__( self, index ):
        return self.resources[ index ]
    
    @property
    def metadata( self ):
        """
        The metadata of the resource

        Returns:
            boolean : status of the pipeline instance

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-02-28
        """
        return self._metadata

    @metadata.setter
    def metadata( self, metadata ):
        """
        Sets the metadata attribute of the resource

        Author: Uwe Sikora <sikora@sub.uni-goettingen.de> ©2020
        Date: 2020-02-28
        """
        self._metadata = metadata