from setuptools import setup

setup(
   name='aggrepy',
   version='1.0.0',
   description='A module to virtually aggregate resources and to load them dynamically',
   author='Uwe Sikora',
   author_email='sikora@sub.uni-goettingen.de',
   packages=['aggrepy'], 
   install_requires=['requests', 'smart-open'], #external packages as dependencies
)